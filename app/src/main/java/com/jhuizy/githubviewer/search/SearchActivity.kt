package com.jhuizy.githubviewer.search

import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import com.github.salomonbrys.kodein.KodeinInjected
import com.github.salomonbrys.kodein.KodeinInjector
import com.github.salomonbrys.kodein.instance
import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import com.jakewharton.rxbinding2.support.v7.widget.queryTextChanges
import com.jakewharton.rxbinding2.view.clicks
import com.jhuizy.githubviewer.GithubApplication
import com.jhuizy.githubviewer.R
import com.jhuizy.githubviewer.common.BasePresenter
import com.jhuizy.githubviewer.common.BaseView
import com.jhuizy.githubviewer.common.hide
import com.jhuizy.githubviewer.common.show
import com.jhuizy.githubviewer.repos.GithubRepoAdapter
import com.jhuizy.githubviewer.repos.Owner
import com.jhuizy.githubviewer.repos.Repo
import com.jhuizy.githubviewer.repos.RepoService
import com.squareup.picasso.Picasso
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_search.*
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SearchModel(val query: String, val state: State) {

    sealed class State {

        object Loading : State()

        sealed class Error : State() {

            object NoInternet : Error()
            object Unknown : Error()

        }

        class Data(val data: List<Repo> = emptyList(), val isPullToRefreshLoading: Boolean = false) : State()

    }
}

interface SearchView : BaseView {

    // intents
    fun searchIntent(): Flowable<String>

    fun refreshIntent(): Flowable<Unit>

    fun retryIntent(): Flowable<Unit>

    // views
    fun update(model: SearchModel)

}

interface SearchService {

    fun search(query: String): Single<List<Repo>>

}

class MockSearchService : SearchService {

    val repos = (0..20).map { i -> Repo(i, Owner("Owner #${i % 5}", i % 5, "url", "url"), "Repo #$i", "Description for repo #$i") }

    var counter = 0

    override fun search(query: String): Single<List<Repo>> {
        return Observable.fromIterable(repos)
                .filter { it.name.toLowerCase().contains(query.toLowerCase()) || it.description.toLowerCase().contains(query.toLowerCase()) }
                .toList()
                .map { if (counter++ % 5 == 0) throw Exception() else it }
                .delay(2, TimeUnit.SECONDS)
    }
}

class GithubUserSearchService(val repoService: RepoService) : SearchService {
    override fun search(query: String): Single<List<Repo>> {
        return repoService.getPublicRepos(query)
                .toList()
                .subscribeOn(Schedulers.io())
    }
}

class SearchPresenter(val searchService: SearchService) : BasePresenter<SearchView> {

    private var lastData: List<Repo>? = null
    private var currentModel: SearchModel = SearchModel("", SearchModel.State.Loading)
    private var disposable: Disposable? = null

    override fun attach(view: SearchView) {

        fun nextSearch(query: String, isPullToRefreshLoading: Boolean): Flowable<SearchModel> =
                searchService.search(query)
                        .map { repos -> SearchModel.State.Data(repos) as SearchModel.State }
                        .onErrorReturn { error -> Timber.e(error); SearchModel.State.Error.NoInternet }
                        .toFlowable()
                        .map { state -> SearchModel(query, state) }
                        .startWith(
                                when {
                                    isPullToRefreshLoading -> SearchModel(query, SearchModel.State.Data(lastData ?: emptyList(), true))
                                    else -> SearchModel(query, SearchModel.State.Loading)
                                }
                        )

        disposable = Flowable.merge(
                view.searchIntent().map { Pair(it, false) },
                Flowable.merge(
                        view.refreshIntent().map { true },
                        view.retryIntent().map { false }
                ).map { Pair(currentModel.query, it) }
        )
                .switchMap { queryIsPullToRefreshPair -> nextSearch(queryIsPullToRefreshPair.first, queryIsPullToRefreshPair.second) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { model: SearchModel ->
                    currentModel = model

                    val state = currentModel.state
                    if (state is SearchModel.State.Data) {
                        lastData = state.data
                    }

                    view.update(model)
                }
    }

    override fun detach() {
        disposable?.dispose()
        disposable = null
    }
}

class SearchActivity : AppCompatActivity(), SearchView {

    private val injector = KodeinInjector()

    private val picasso: Picasso by lazy { Picasso.with(this) }
    private val searchTextChangesSubject: PublishSubject<String> by lazy { PublishSubject.create<String>() }

    private var presenter: BasePresenter<SearchView>? = null
    private var adapter: GithubRepoAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setSupportActionBar(toolbar)

        if (adapter == null) {
            adapter = GithubRepoAdapter(picasso) { view, repo -> }
        }

        recyclerview_repos.adapter = adapter
        recyclerview_repos.layoutManager = LinearLayoutManager(this)

        val kodein = (application as GithubApplication).kodein

        if (presenter == null) {
            presenter = SearchPresenter(GithubUserSearchService(kodein.instance()))
        }

        presenter?.attach(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_search, menu)

        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.search)) as? android.support.v7.widget.SearchView
        if (searchView != null) {
            searchView.queryTextChanges()
                    .debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .map { it.toString() }
                    .subscribe { searchTextChangesSubject.onNext(it) }

        }


        return true
    }

    override fun searchIntent(): Flowable<String> {
        return searchTextChangesSubject.toFlowable(BackpressureStrategy.DROP)
    }

    override fun refreshIntent(): Flowable<Unit> {
        return swipe_refresh.refreshes()
                .subscribeOn(AndroidSchedulers.mainThread())
                .toFlowable(BackpressureStrategy.DROP)
    }

    override fun retryIntent(): Flowable<Unit> {
        return button_no_internet.clicks()
                .subscribeOn(AndroidSchedulers.mainThread())
                .toFlowable(BackpressureStrategy.BUFFER)
    }

    override fun update(model: SearchModel) {
        when (model.state) {
            is SearchModel.State.Loading -> {
                layout_loading.show()
                recyclerview_repos.hide()
                layout_no_internet.hide()
                swipe_refresh.isRefreshing = false
            }
            is SearchModel.State.Error -> {
                layout_loading.hide()
                recyclerview_repos.hide()
                layout_no_internet.show()
                swipe_refresh.isRefreshing = false

            }
            is SearchModel.State.Data -> {
                layout_loading.hide()
                layout_no_internet.hide()
                recyclerview_repos.show()
                swipe_refresh.isRefreshing = model.state.isPullToRefreshLoading

                adapter?.add(model.state.data)
            }
        }
    }
}