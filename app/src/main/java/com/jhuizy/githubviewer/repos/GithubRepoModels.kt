package com.jhuizy.githubviewer.repos

data class Repo(
        val id: Int,
        val owner: Owner,
        val name: String,
        val description: String
)

data class Owner(
        val login: String,
        val id: Int,
        val avatarUrl: String,
        val url: String
)