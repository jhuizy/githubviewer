package com.jhuizy.githubviewer.repos

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.jhuizy.githubviewer.BitmapFetcher
import com.jhuizy.githubviewer.R
import com.jhuizy.githubviewer.SimpleBitmapFetcher
import com.squareup.picasso.Picasso
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

class GithubRepoActivity : AppCompatActivity(), GithubRepoListFragment.Callback {

    override val kodein by lazy {
        Kodein {
            bind<ObjectMapper>() with singleton { ObjectMapper().apply { configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false) } }
            bind<Retrofit>() with singleton {
                Retrofit.Builder()
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(JacksonConverterFactory.create(instance()))
                        .baseUrl("https://api.github.com")
                        .build()
            }

            bind<RepoService>() with singleton { RetrofitRepoService(instance()) }
            bind<BitmapFetcher>() with singleton { SimpleBitmapFetcher(Picasso.with(this@GithubRepoActivity)) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_github_repos)

        val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_LIST) ?: GithubRepoListFragment.newInstance("jhuizy").apply {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.layout_content, this, FRAGMENT_LIST)
                    .commit()
        }
    }

    companion object {

        val FRAGMENT_LIST = "list_fragment"
        val FRAGMENT_DETAIL = "detail_fragment"

    }

}