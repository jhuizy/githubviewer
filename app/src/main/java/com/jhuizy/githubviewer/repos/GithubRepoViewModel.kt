package com.jhuizy.githubviewer.repos

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable

class GithubRepoViewModel(
        val id: Int,
        val title: String,
        val avatar: Bitmap,
        val owner: String,
        val description: String
) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<GithubRepoViewModel> = object : Parcelable.Creator<GithubRepoViewModel> {
            override fun createFromParcel(source: Parcel): GithubRepoViewModel = GithubRepoViewModel(source)
            override fun newArray(size: Int): Array<GithubRepoViewModel?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readInt(), source.readString(), source.readParcelable<Bitmap>(Bitmap::class.java.classLoader), source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeString(title)
        dest?.writeParcelable(avatar, 0)
        dest?.writeString(owner)
        dest?.writeString(description)
    }
}