package com.jhuizy.githubviewer.repos

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.jhuizy.githubviewer.R
import kotlinx.android.synthetic.main.fragment_github_repo_details.*
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.support.v4.withArguments

class GithubRepoDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_github_repo_details, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val repo = arguments.getParcelable<GithubRepoViewModel>(ARG_GITHUB_REPO)

        val act = activity as AppCompatActivity
        act.setSupportActionBar(toolbar)
        act.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        act.supportActionBar?.setTitle(repo.title)

        image_avatar.imageBitmap = repo.avatar
        text_description.text = repo.description
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            android.R.id.home -> {
                activity.onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {

        const val ARG_GITHUB_REPO = "arg_github_repo"

        fun newInstance(githubRepoViewModel: GithubRepoViewModel) : GithubRepoDetailFragment {
            return GithubRepoDetailFragment().apply {
                withArguments(ARG_GITHUB_REPO to githubRepoViewModel)
                retainInstance = true
                setHasOptionsMenu(true)
            }
        }

    }
}