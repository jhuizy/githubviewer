package com.jhuizy.githubviewer.repos

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path

interface RepoService {

    fun getPublicRepos(username: String): Observable<Repo>

}

class RetrofitRepoService(retrofit: Retrofit) : RepoService {

    private val repoRetrofitService = retrofit.create(RepoRetrofitService::class.java)

    override fun getPublicRepos(username: String): Observable<Repo> {
        return repoRetrofitService.getPublicRepos(username)
                .flattenAsObservable { it }
                .map { it.toRepo() }
    }

    interface RepoRetrofitService {

        @GET("users/{username}/repos")
        fun getPublicRepos(@Path("username") username: String): Single<List<RepoEnvelope>>

    }

    private fun RepoEnvelope.toRepo() = Repo(id!!, owner!!.toOwner(), name!!, description ?: "")

    private fun OwnerEnvelope.toOwner() = Owner(login!!, id!!, avatar_url!!, url!!)

    data class RepoEnvelope(
            var id: Int? = null,
            var owner: OwnerEnvelope? = null,
            var name: String? = null,
            var description: String? = null
    )

    data class OwnerEnvelope(
            var login: String? = null,
            var id: Int? = null,
            var avatar_url: String? = null,
            var url: String? = null
    )
}


