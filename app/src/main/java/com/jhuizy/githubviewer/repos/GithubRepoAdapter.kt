package com.jhuizy.githubviewer.repos

import android.support.v4.view.ViewCompat
import android.support.v7.util.SortedList
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.util.SortedListAdapterCallback
import android.view.View
import android.view.ViewGroup
import com.jhuizy.githubviewer.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.github_repo_item.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

class GithubRepoAdapter(val picasso: Picasso, val onItemClicked: (View, Repo) -> Unit) : RecyclerView.Adapter<GithubRepoAdapter.MyViewHolder>() {

    private val sortedList: SortedList<Repo> = SortedList(Repo::class.java, MyCallback(this))

    fun add(items: List<Repo>) {
        sortedList.beginBatchedUpdates()
        sortedList.clear()
        sortedList.addAll(items)
        sortedList.endBatchedUpdates()
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        holder?.apply {
            val repo = sortedList[position]
            title.text = repo.name
            owner.text = repo.owner.login
            description.text = repo.description

//            picasso.load(repo.owner.avatarUrl)
//                    .into(avatar)

            itemView.onClick { onItemClicked(itemView, sortedList[adapterPosition]) }

            ViewCompat.setTransitionName(avatar, "image_transition_$position")
        }
    }

    override fun getItemCount(): Int {
        return sortedList.size()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        val view = parent!!.context.layoutInflater.inflate(R.layout.github_repo_item, parent, false)
        return MyViewHolder(view)
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val title = view.text_title
        val avatar = view.image_avatar
        val owner = view.text_owner
        val description = view.text_description

    }


    class MyCallback(adapter: RecyclerView.Adapter<*>) : SortedListAdapterCallback<Repo>(adapter) {
        override fun areItemsTheSame(item1: Repo?, item2: Repo?): Boolean {
            return item1?.id == item2?.id
        }

        override fun compare(o1: Repo?, o2: Repo?): Int {
            return when {
                o1 == null && o2 == null -> 0
                o1 == null -> 1
                o2 == null -> -1
                else -> o1.name.compareTo(o2.name)
            }
        }

        override fun areContentsTheSame(oldItem: Repo?, newItem: Repo?): Boolean {
            return oldItem == newItem
        }
    }
}