package com.jhuizy.githubviewer.repos

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.transition.*
import android.view.*
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinInjector
import com.github.salomonbrys.kodein.instance
import com.jhuizy.githubviewer.BitmapFetcher
import com.jhuizy.githubviewer.R
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_github_repo_list.*
import kotlinx.android.synthetic.main.github_repo_item.view.*
import org.jetbrains.anko.doFromSdk
import org.jetbrains.anko.support.v4.withArguments

class GithubRepoListFragment : Fragment() {

    private val injector = KodeinInjector()

    private val repoService: RepoService by injector.instance()
    private val bitmapFetcher: BitmapFetcher by injector.instance()

    private var adapter: GithubRepoAdapter? = null
    private lateinit var callback: Callback

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callback = context!! as Callback
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.fragment_github_repo_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val act = activity as AppCompatActivity
        act.setSupportActionBar(toolbar)
        act.supportActionBar?.title = "Search"
        act.supportActionBar?.setDisplayShowHomeEnabled(false)

        injector.inject(callback.kodein)

        if (adapter == null) {
            adapter = GithubRepoAdapter(Picasso.with(context)) { view, item ->  }

            val username = arguments!!.getString(ARG_USERNAME)!!

            loadItems(username)
        }

        recyclerview_repos.adapter = adapter
        recyclerview_repos.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.fragment_github_repo_details, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun loadItems(username: String) {
        repoService.getPublicRepos(username)
                .flatMapSingle { repo ->
                    bitmapFetcher.fetch(repo.owner.avatarUrl)
                            .map { bitmap ->
                                GithubRepoViewModel(repo.id, repo.name, bitmap, repo.owner.login, repo.description)
                            }
                }
                .observeOn(AndroidSchedulers.mainThread())
                //.subscribe { adapter?.add(it) }
    }

    private fun onItemSelected(view: View, githubRepoViewModel: GithubRepoViewModel) {

        val fragment = GithubRepoDetailFragment.newInstance(githubRepoViewModel)

        doFromSdk(Build.VERSION_CODES.LOLLIPOP) {

            val transition = TransitionSet()
                    .setOrdering(TransitionSet.ORDERING_TOGETHER)
                    .addTransition(ChangeBounds())
                    .addTransition(ChangeImageTransform())
                    .addTransition(ChangeTransform())

            fragment.sharedElementEnterTransition = transition
            fragment.sharedElementReturnTransition = transition
            fragment.enterTransition = Fade()
            this.exitTransition = Fade()
        }

        val act = activity as AppCompatActivity
        act.supportFragmentManager.beginTransaction()
                .replace(R.id.layout_content, fragment, "FRAG_DETAILS")
                .addToBackStack(null)
                .addSharedElement(view.image_avatar, getString(R.string.transition_image_avatar))
                .commit()
    }

    interface Callback {
        val kodein: Kodein
    }

    companion object {

        val ARG_USERNAME = "arg_username"

        fun newInstance(username: String) = GithubRepoListFragment().apply {
            retainInstance = true
            withArguments(ARG_USERNAME to username)
            setHasOptionsMenu(true)
        }

    }

}