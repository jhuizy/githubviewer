package com.jhuizy.githubviewer

import android.graphics.Bitmap
import com.squareup.picasso.Picasso
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

interface BitmapFetcher {
    fun fetch(imageUrl: String): Single<Bitmap>
}

class SimpleBitmapFetcher(val picasso: Picasso) : BitmapFetcher {
    override fun fetch(imageUrl: String): Single<Bitmap> {
        return Single
                .fromCallable { picasso.load(imageUrl).get() }
                .subscribeOn(Schedulers.io())
    }
}