package com.jhuizy.githubviewer

import com.jhuizy.githubviewer.repos.Owner
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {

    @GET("users/{username}")
    fun getUser(@Path("username") username: String): Single<Owner>

}