package com.jhuizy.githubviewer

import android.app.Application
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.jhuizy.githubviewer.repos.RepoService
import com.jhuizy.githubviewer.repos.RetrofitRepoService
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import timber.log.Timber

class GithubApplication : Application() {

    val kodein by lazy {
        Kodein {
            bind<ObjectMapper>() with singleton { ObjectMapper().apply { configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false) } }
            bind<Retrofit>() with singleton {
                Retrofit.Builder()
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(JacksonConverterFactory.create(instance()))
                        .baseUrl("https://api.github.com")
                        .build()
            }

            bind<RepoService>() with singleton { RetrofitRepoService(instance()) }
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }

}