package com.jhuizy.githubviewer.common

interface BaseView {

}

interface BasePresenter<in V : BaseView> {

    fun attach(view: V)

    fun detach()

}
