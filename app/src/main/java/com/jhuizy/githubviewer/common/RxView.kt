package com.jhuizy.githubviewer.common

import android.view.View
import android.widget.TextView
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import org.jetbrains.anko.onClick
import org.jetbrains.anko.textChangedListener

fun TextView.textChanges(): Flowable<CharSequence> = Flowable.create(
        { emitter -> this.textChangedListener { onTextChanged { charSequence, a, b, c -> emitter.onNext(charSequence) } } },
        BackpressureStrategy.BUFFER
)

fun View.clicks(): Flowable<Unit> = Flowable.create(
        { emitter -> this.onClick { emitter.onNext(Unit) }},
        BackpressureStrategy.BUFFER
)
